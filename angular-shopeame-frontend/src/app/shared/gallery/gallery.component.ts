import { EditService } from './../services/local/edit.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() productList: any;

  constructor(private editService: EditService) { }

  ngOnInit(): void {
  }

}
