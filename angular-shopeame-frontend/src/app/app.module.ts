import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RatingModule} from 'primeng/rating';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { GalleryComponent } from './shared/gallery/gallery.component';
import { GestionComponent } from './pages/gestion/gestion.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { SearchBarComponent } from './shared/search-bar/search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductCardComponent } from './shared/product-card/product-card.component';
import { StarRatingModule } from 'angular-star-rating';
import { GalleryTableComponent } from './shared/gallery-table/gallery-table.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    FooterComponent,
    ProductosComponent,
    GalleryComponent,
    GestionComponent,
    SearchBarComponent,
    ProductCardComponent,
    GalleryTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule,
    RatingModule,
    StarRatingModule.forRoot(),
    TableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
