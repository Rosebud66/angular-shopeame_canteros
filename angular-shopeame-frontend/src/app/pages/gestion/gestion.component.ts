import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Output } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.scss'],
})
export class GestionComponent implements OnInit {
  @Output() productForm;

  submited = false;
  product: any = {
    id: '',
    name: '',
    price: '',
    description: '',
    stars: '',
    image: '',
  };
  id: any;

  constructor(
    private servicesService: ServicesService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.route.paramMap.subscribe((params) => {
      params.get('id') ? (this.id = params.get('id')) : null;
    });
    this.servicesService.getProduct(this.id).subscribe((res: any) => {
      this.product = res;
    });

    this.productForm = this.formBuilder.group({
      name: [''],
      price: [''],
      description: [''],
      stars: [''],
      image: [''],
    });
  }

  ngOnInit(): void {}

  productMe() {
    this.submited = true;
    return this.productForm;
  }

  guardarProducto() {
    if(this.product.id !== ''){
      this.servicesService.putProduct(this.product).subscribe((res) => {
        console.log(res);
      });
    }else{
      this.servicesService.postProduct(this.product).subscribe((res) => {});
      }   
  }

  borrarProducto() {
    this.servicesService.deleteProduct(this.product).subscribe((res) => {
      this.product.id = '';
      this.product.name = '';
      this.product.price = '';
      this.product.description = '';
      this.product.image = '';
      this.product.stars = '';
      console.log(res);
    });
  }

  
}
