import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  productList: any;
  showTable: any = false;
  public myClass = true;

  constructor(private servicesService: ServicesService) { }

  ngOnInit(): void {
    this.servicesService.getProducts().subscribe((res: any) => {
      this.productList = res;
    });
  }
  filterEmitter(changes : any){
    this.servicesService.getProducts().subscribe((res: any) => {
      this.productList = res.filter( (current : any) =>{
        return (current.name.toLowerCase().indexOf(changes.toLowerCase()) === -1) ? false : true;
      });
    });
  }

  showLista(){
    this.showTable = false;
    this.myClass = true;
  } 

  showTabla(){
    this.showTable = true;
    this.myClass = !this.myClass;
  } 


}

