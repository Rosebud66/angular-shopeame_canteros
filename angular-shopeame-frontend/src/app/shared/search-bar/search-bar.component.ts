import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  formGroupSearchForm: any;

  @Output() filterEmitter = new EventEmitter();

  //searchValue$;
  textoDeInput='';


  constructor(private formBuilder: FormBuilder) {
    this.formGroupSearchForm = this.formBuilder.group({
      name: ['']
    });

    // this.searchValue$ = this.formGroupSearchForm.valueChanges.subscribe((changes : any) => {
    //   this.filterEmitter.emit(changes);
    // });
   }

  ngOnInit(): void {
  }

  
  buscar(item : any){
    if(item.key === 'Enter'){
      this.filterEmitter.emit(this.textoDeInput);
    }
    
  }
}
