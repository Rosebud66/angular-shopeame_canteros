import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  @Input() productForm: any;
  constructor(private http: HttpClient) { }

  getProducts(){
    return this.http.get('http://localhost:3000/products');
  }
  getProduct(id : any){
    return this.http.get('http://localhost:3000/products/' + id);
  }
  postProduct(product: any) {
    delete product.id;
    console.log(product);
    return this.http.post('http://localhost:3000/products/', product);
  }
  
  deleteProduct(product: any){
    return this.http.delete('http://localhost:3000/products/' + product.id);
  }

  putProduct(product: any) {
    return this.http.put('http://localhost:3000/products/'+ product.id, product);
  }
}
