import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditService {

  private edit: any = [];

  constructor() { }

    editNew(newEdit: any){
      if (this.edit === []){
        return this.edit.push(newEdit);
      }
        else{
        return this.edit.splice(0, 1, newEdit);
        }
      
    }

  getEdit(){
    return this.edit;
  }

}
