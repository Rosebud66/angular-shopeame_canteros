import { Component, Input, OnInit } from '@angular/core';
import { EditService } from '../services/local/edit.service';

@Component({
  selector: 'app-gallery-table',
  templateUrl: './gallery-table.component.html',
  styleUrls: ['./gallery-table.component.scss']
})
export class GalleryTableComponent implements OnInit {

  @Input() productList: any;

  constructor() { }

  ngOnInit(): void {
  }

}
